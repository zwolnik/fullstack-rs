# Rust fullstack app template with Yew and Rocket

This is a template of a Rust fullstack application. It has
 - Yew as frontend
 - Rocket as backend (replacable)
 - CI
 - Docker
 - unit testing

Feel free to contribute if you have any ideas for improving starting experience with fullstack rust

![](https://i.ibb.co/yYYZyzp/fullstack-rs.png)

## Setup

Install toolchain for wasm frontend
```bash
rustup target add wasm32-unknown-unknown
```

Install required tools
```bash
cargo install trunk wasm-bindgen-cli wasm-pack # add 'cargo-watch' for hot-reload
```

Copy sample configuration
```bash
cp .env.sample .env
```

You can now run this service with
```bash
cargo run -p back --release
```

## Run

To spin up a server with hot-reload for development run
```bash
cargo watch -x 'run -p back'
```

## Building frontend and backend separately

Backend is just a regular cargo-based project however frontend requires special treatment. Frontend project if built alone with eg. `cargo build -p front` will by default build with `cargo` for your host architecture. Frontend has to be built for `wasm32-unknown-unknown` and bundled with some js-launcher script and `index.html`. This is handled by a `trunk` tool designed to work with yew. Under the hood backend project has a `build.rs` which will use `trunk` to rebundle frontend on each change of it's production sources. Remember that if you add any new static file you'll need to add new rule for cargo to watch it for changes in `build.rs`.

To bundle frontend use
```bash
cd front
trunk build --release
```

To serve just a frontend with hot-reloading use
```bash
cd front
trunk serve
```

## Tests

Executing tests for backend works with regular `cargo test`, however it will unnecessary build frontend for host architecture unless you add tests marked with `#[test]` in it. As this template doesn't have any you can use:
```
cargo test -p back
```

Frontend tests use `#[wasm_bindgen_test]` to execute in a headless browser thus they have to be ran with `wasm-pack`. It will require you to install firefox or/and chromium/chrome and chromium-driver.
```
wasm-pack test --headless --firefox --chrome front
```

## Docker image
This repository uses [fullstack-rs](https://gitlab.com/zwolnik/fullstack-rs-docker) docker image for builds and tests. You can create your own or use this pushed to [docker hub](https://hub.docker.com/r/zwo1in/fullstack-rs).

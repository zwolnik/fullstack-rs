use yew::prelude::*;

#[function_component(Model)]
fn model() -> Html {
    let counter = use_state(|| 0);
    let onclick = {
        let counter = counter.clone();
        Callback::from(move |_| counter.set(*counter + 1))
    };
    html! {
        <>
            <Logo />
            <Heading />
            <div class="panel">
                <p>{ *counter }</p>
                <button {onclick}>{ "+1" }</button>
            </div>
        </>
    }
}

#[function_component(Heading)]
fn heading() -> Html {
    html! {
        <div class="heading">
            <h1>{ "Fullstack Rust application" }</h1>
            <h3>{ "Built with Yew and Actix-web" }</h3>
        </div>
    }
}

#[function_component(Logo)]
fn logo() -> Html {
    let alt = "Rust language official logo";
    let src = "/static/Rust_programming_language_black_logo.svg";
    html! {
        <img class="logo" {src} {alt} />
    }
}

fn main() {
    yew::start_app::<Model>();
}

#[cfg(test)]
pub mod tests {
    use super::*;
    use wasm_bindgen::JsCast;
    use wasm_bindgen_test::wasm_bindgen_test;
    use web_sys::HtmlElement;

    // This macro should be only called once per binary in crate
    wasm_bindgen_test::wasm_bindgen_test_configure!(run_in_browser);

    #[wasm_bindgen_test]
    fn clicking_on_button_should_increment_value() {
        let document = gloo_utils::document();
        let body = document.body().unwrap();
        let div = document.create_element("div").unwrap();
        body.append_child(&div).unwrap();
        yew::start_app_in_element::<Model>(div);

        let value = body.query_selector(".panel > p").unwrap().unwrap();
        let button = body.query_selector(".panel > button").unwrap().unwrap();
        let button = button.dyn_into::<HtmlElement>().unwrap();

        assert_eq!("0", value.text_content().unwrap());
        button.click();
        assert_eq!("1", value.text_content().unwrap());
    }
}
